package com.homelane.controller;

import java.net.URI;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.homelane.pojo.Note;
import com.homelane.representation.NoteRepresentation;
import com.homelane.service.NoteService;

@Controller
@RequestMapping("/notes")
public class NoteResource {
	private static final Logger log = LogManager.getLogger(NoteResource.class);
	@Autowired
	private NoteService service;
	
	
	//add a note
	 @PostMapping
	    public ResponseEntity addNote(@RequestBody NoteRepresentation note) {
		 try{
			 if(note.getText().isEmpty() || note.getText().equals("") ||
					 note.getText() == null || note.getTitle().isEmpty() || note.getTitle().equals("") ||
					 note.getTitle() == null || note.getText().equals("") ||note.getTitle().equals(""))
				 return ResponseEntity.badRequest().body("TEXT and TITLE cannot be empty"); 
				 
				Note result=service.Add(note);
				 URI location = ServletUriComponentsBuilder
		                    .fromCurrentRequest().path("/{id}")
		                    .buildAndExpand(result.getId()).toUri();
				return ResponseEntity.created(location).body(result); 
			}catch(Exception e){
				log.error("addNote",e);
				return ResponseEntity.status(500).build();	
			}
	 }
	 
	 
	 @GetMapping("{id}")
		@ResponseBody
		public ResponseEntity getNote( @PathVariable("id") String id) {
			try{  
				Note type=service.getNote(id);
				if(type==null)
					return ResponseEntity.noContent().build();
				return ResponseEntity.ok().body(type); 
			}catch(Exception e){
				log.error("getNote Exception : ",e);
				return ResponseEntity.status(500).build();	
			}

		}
	 //ist all notes
	 @GetMapping
		@ResponseBody
		public ResponseEntity getNotes( ) {
			try{  
				List<Note> type=service.getNotes();
				if(type==null)
					return ResponseEntity.noContent().build();
				return ResponseEntity.ok().body(type); 
			}catch(Exception e){
				log.error("getNote Exception : ",e);
				return ResponseEntity.status(500).build();	
			}

		}
	       
	 //delete node
	 @DeleteMapping("{id}")
		@ResponseBody
		public ResponseEntity deleteNotes(@PathVariable("id") String id ) {
			try{  
				service.DeleteNote(id);
				
				return ResponseEntity.ok().build(); 
			}catch(Exception e){
				log.error("getNote Exception : ",e);
				return ResponseEntity.status(500).build();	
			}

		}
	 
	 
	 //search in title,created,level
	 @GetMapping("searchBy/{search}")
		@ResponseBody
		public ResponseEntity searchNote( @PathVariable("search") String search) {
			try{  
				List<Note> type=service.searchNotes(search);
				if(type==null)
					return ResponseEntity.noContent().build();
				return ResponseEntity.ok().body(type); 
			}catch(Exception e){
				log.error("getNote Exception : ",e);
				return ResponseEntity.status(500).build();	
			}

		}
	 //search text
	 @GetMapping("search-in-Text/{search}")
		@ResponseBody
		public ResponseEntity searchNoteText( @PathVariable("search") String search) {
			try{  
				List<Note> type=service.searchNoteText(search);
				if(type==null)
					return ResponseEntity.noContent().build();
				return ResponseEntity.ok().body(type); 
			}catch(Exception e){
				log.error("getNote Exception : ",e);
				return ResponseEntity.status(500).build();	
			}

		}
	 
	 //edit
	 @PutMapping()
		@ResponseBody
		public ResponseEntity editNote( @RequestBody NoteRepresentation note) {
			try{  
				 if(note.getText().isEmpty() || note.getText().equals("") ||
						 note.getText() == null || note.getTitle().isEmpty() || note.getTitle().equals("") ||
						 note.getTitle() == null || note.getText().equals("") ||note.getTitle().equals(""))
					 return ResponseEntity.badRequest().body("TEXT and TITLE cannot be empty"); 
					 
				List<Note> type=service.editNote(note);
				if(type==null)
					return ResponseEntity.noContent().build();
				return ResponseEntity.ok().body(type); 
			}catch(Exception e){
				log.error("getNote Exception : ",e);
				return ResponseEntity.status(500).build();	
			}

		}
	       
}
