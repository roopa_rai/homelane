package com.homelane.main;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.homelane.pojo.Note;

@SpringBootApplication
@ComponentScan(basePackages = "com.homelane")
@EnableAutoConfiguration
@Configuration
public class HomelaneApplication {
	@Bean
	public List<Note>  notes() {
	    final List<Note> notes = new ArrayList<Note>();
	    
	    return notes;
	}
	
	public static void main(String[] args) {
		SpringApplication.run(HomelaneApplication.class, args);
	}

}

