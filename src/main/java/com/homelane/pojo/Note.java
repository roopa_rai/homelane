package com.homelane.pojo;

import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;

import org.apache.logging.log4j.core.util.datetime.Format;

public class Note {
	private String id;
	private String title;
	private String text;
	private long created;
	private Level level;
	private String color;
	public String getId() {
		return this.id;
	}
	public void setId() {
		this.id = UUID.randomUUID().toString();
			
	}
	public void setOldId(String Id) {
		this.id = Id;
			
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getCreated() {
		 Date date = new Date(this.created);
		 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 return format.format(date);
	}
	public void setCreated(Timestamp created) {
		Instant instant = Instant.now();
		this.created = instant.toEpochMilli();
	}
	public Level getLevel() {
		return level;
	}
	public void setLevel(Level level) {
		this.level = level;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
}
