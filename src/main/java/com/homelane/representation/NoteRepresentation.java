package com.homelane.representation;

public class NoteRepresentation {
private String id;
private String title;
private String text;
private String level;
private String color;
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getText() {
	return text;
}
public void setText(String text) {
	this.text = text;
}
public String getLevel() {
	return level;
}
public void setLevel(String level) {
	this.level = level;
}
public String getColor() {
	return color;
}
public void setColor(String color) {
	this.color = color;
}	
}
