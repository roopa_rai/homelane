package com.homelane.service;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.homelane.pojo.Note;
import com.homelane.representation.NoteRepresentation;
import com.homelane.util.Mapper;


@Service
public class NoteService {
	private static final Log log = LogFactory.getLog(NoteService.class);
	
	@Resource(name = "notes")
	private List<Note> notes;
	
	
	public Note Add(NoteRepresentation rep) {
		Note note=Mapper.toNote(rep);
		note.setId();
		notes.add(note);
		return note;
	}
	
	public void delete(Note note) {
		notes.remove(note);
	}
	
	public List getNotes() {
		return notes;
	}
	
	public Note getNote(String id) {
		Note note=notes.stream().filter(x-> x.getId().equals(id)).collect(Collectors.toList()).get(0);
		return note;
	}

	public void DeleteNote(String id) {
		notes.remove(this.getNote(id));
	}

	public List searchNotes(String search) {
		return notes.stream().filter(x -> x.getTitle().contains(search) ||
				x.getLevel().toString().contains(search.toUpperCase()) || x.getCreated().contains(search))
				.collect(Collectors.toList());
	}
	
	
	public List searchNoteText(String search) {
		return notes.stream().filter(x -> x.getText().contains(search)).collect(Collectors.toList());
	}

	public List editNote(NoteRepresentation rep) {
		
		return notes.stream().map(x -> {
			Note note=Mapper.toNote(rep);
			if(x.getId().equals(note.getId())) {
				x.setColor(note.getColor());
				x.setLevel(note.getLevel());
				x.setText(note.getText());
				x.setTitle(note.getTitle());
			}
			return x;
		}).collect(Collectors.toList());
		
	}
}
