package com.homelane.util;

import com.homelane.pojo.Level;
import com.homelane.pojo.Note;
import com.homelane.representation.NoteRepresentation;

public class Mapper {

	public static Note toNote(NoteRepresentation rep) {
		Note note = new Note();
		note.setOldId(rep.getId());
		note.setColor(rep.getColor());
		note.setText(rep.getText());
		note.setTitle(rep.getTitle());
		if (rep.getLevel().equalsIgnoreCase("medium"))
			note.setLevel(Level.MEDIUM);
		else if (rep.getLevel().equalsIgnoreCase("high"))
			note.setLevel(Level.HIGH);
		else
			note.setLevel(Level.LOW);//default value for level
		
		return note;
	}
	
}
